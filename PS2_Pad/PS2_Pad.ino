/*
PS2Controlv0.ino
21 September 2015
Hamish Trolove - www.techmonkeybusiness.com

This sketch illustrates the use of a Playstation 2 Controller to
actuate a series of servos and an Electronic Speed Controller (ESC)
as you might do with a vehicle.  An LED light is used to illustrate
the use of the Playstation 2 Buttons.

Pin assignments are:

3.3V output to PS2 red pin
5V output to 1Kohm pull up resistors for PS2.
Pin D02 to PS2 brown pin (data)
Pin D03 to PS2 yellow pin (attention)
Pin D04 to PS2 orange pin (command)
Pin D05 to PS2 blue pin (clock)

Pin D06 to ESC Signal Pin  
Pin D07 to Steering Servo Signal pin
Pin D08 to Servo 1 Signal pin
Pin D09 to Servo 2 Signal pin

Pin D13 to LED Transistor Base

The ESC servo connection supplies 5.6V to the servos.

The coding pulls on the PS2X library developed by Bill Porter.
See www.billporter.info for the latest from Bill Porter and to
download the library.

The controls used for this sketch are;
Right Stick - X-axis = Steering Servo left/right, Y-axis = ESC forward/backward
Left Stick - X-axis = Servo 2 left/right, Y-axis = Servo 1 left/right

Triangle = Toggle the LED
*/


#include <Servo.h>  //For driving the ESCs and Servos
#include <PS2X_lib.h> // Bill Porter's PS2 Library


#define MOTOR_1_PWM  (6)  //D6
#define MOTOR_1_DIR  (7)  //D7

#define MOTOR_2_PWM  (10)  //D10
#define MOTOR_2_DIR  (9)  //D9

#define WINDLASS_SERVO  (12)  //D12
#define WINDLASS_STOP   86
#define WINDLASS_POWER  A1    //A1

#define LIGHTS          (11)  //D11

#define GUN_FIRE        A0  //A0

#define STOP    (0)
#define FORWARD (1)
#define BACKWARD (2)

//AUDIO
#define NEXT_VUP        A3
#define PREV_VDOWN      A2
#define PP              A5
#define REP             A4 

PS2X ps2x;  //The PS2 Controller Class
Servo SteeringServo;  //Create servo object representing SteeringServo
Servo ServoN1;  //Create servo object representing Servo 1
Servo ServoN2;  //Create servo object representing Servo 2
Servo ESCcontrol;  //Create servo object representing ESC
Servo windlass;   //Create servo object for Windlass

const int LEDpin = 13;  //green LED is on Digital pin 13

volatile boolean LEDHdlts; //LED headlights on/off toggle

int PlyStnRStickUpDn = 0;  //Value read off the PS2 Right Stick up/down.
int PlyStnRStickLtRt = 0;  //Value read off the PS2 Right Stick left/right
int PlyStnLStickUpDn = 0; //Value read off the PS2 Left Stick up/down
int PlyStnLStickLtRt = 0; // Value read off the PS2 Left Stick left/right

int ESCSetting = 90; //Setting for the ESC (degrees).
int StrServoSetting = 90; //Setting for the Steering Servo
int ServoN1Setting = 90; //Setting for the Servo 1
int ServoN2Setting = 90; //Setting for the Servo 2


void audioNext(void)
{

  pinMode(NEXT_VUP, OUTPUT);
  digitalWrite(NEXT_VUP, LOW);
  delay(200);
  pinMode(NEXT_VUP, INPUT);
}
void audioPrev(void)
{
  pinMode(PREV_VDOWN, OUTPUT);
  digitalWrite(PREV_VDOWN, LOW);
  delay(200);
  pinMode(PREV_VDOWN, INPUT);
}
void audioss(void)
{
  pinMode(PP, OUTPUT);
  digitalWrite(PP, LOW);
  delay(200);
  pinMode(PP, INPUT);
}
void audiorep(void)
{
  pinMode(REP, OUTPUT);
  digitalWrite(REP, LOW);
  delay(200);
  pinMode(REP, INPUT);
}
void setup()
{
  ps2x.config_gamepad(5,4,3,2, false, false);
  //setup pins and settings: GamePad(clock, command, attention, data, Pressures, Rumble)
  //We have disabled the pressure sensitivity and rumble in this instance.
  pinMode(LEDpin, OUTPUT);  //Sets the LEDpin to output
  pinMode(MOTOR_1_DIR, OUTPUT);
  pinMode(MOTOR_2_DIR, OUTPUT);  
  pinMode(MOTOR_1_PWM, OUTPUT);
  pinMode(MOTOR_2_PWM, OUTPUT);
  pinMode(WINDLASS_POWER, OUTPUT);
  pinMode(LIGHTS, OUTPUT);
  pinMode(GUN_FIRE, OUTPUT);

  LEDHdlts = false;  //Sets the Headlights to off
  windlass.attach(WINDLASS_SERVO);
 // SteeringServo.attach(7);// attaches the Steering Servo to pin 7
 // ServoN1.attach(8);// attaches the Servo 1 to pin 8
 // ServoN2.attach(9);// attaches the Servo 2 to pin 9
 // ESCcontrol.attach(6,150,2250);// attaches the ESC to pin 6
 // //The ESC attachment command above also includes the signal settings
 // //for the maximum and minimum that the ESC will recognise.  This
 // //varies for different ESCs.

  //Set all ESCs and Servos to a neutral 90 degree position
  //this avoids the ESC trying to calibrate.
 // ESCcontrol.write(90);
 // SteeringServo.write(90);
 // ServoN1.write(90);
 // ServoN2.write(90);
 digitalWrite(LIGHTS, LOW);
 digitalWrite(GUN_FIRE, LOW); 
 digitalWrite(WINDLASS_POWER, LOW);
 windlass.write(WINDLASS_STOP);
 Serial.begin(115200);
  delay(5000); //Five second delay to allow ESC and controller to
               // fully initialise.

}


void motor_1_set (uint8_t dir, uint8_t power)
{
  Serial.print("Left :");
  Serial.println(power);
  if(dir == STOP)
  {
    power = 0;
     digitalWrite(MOTOR_1_DIR, LOW);
  }

  if(dir == FORWARD)
  {
     digitalWrite(MOTOR_1_DIR, LOW);
  }

  if(dir == BACKWARD)
  {
     digitalWrite(MOTOR_1_DIR, HIGH);
     //power = 255 - power;
  }
   analogWrite(MOTOR_1_PWM, power);
}



void motor_2_set (uint8_t dir, uint8_t power)
{
  Serial.print("Right :");
  Serial.println(power);
  if(dir == STOP)
  {
    power = 0;
     digitalWrite(MOTOR_2_DIR, LOW);
  }

  if(dir == FORWARD)
  {
     digitalWrite(MOTOR_2_DIR, LOW);
  }

  if(dir == BACKWARD)
  {
     digitalWrite(MOTOR_2_DIR, HIGH);
     //power = 255 - power;
  }
   analogWrite(MOTOR_2_PWM, power);
}



void loop()
{
  ps2x.read_gamepad(); //This needs to be called at least once a second
                        // to get data from the controller.

  if(ps2x.ButtonPressed(PSB_GREEN))  //Triangle pressed
  {
    LEDHdlts = !LEDHdlts; //Toggle the LED light flag
  }

  
  if (ps2x.ButtonPressed(PSB_PAD_UP))
  {
    windlass.write(0);
    digitalWrite(WINDLASS_POWER, HIGH);
  }
  if (ps2x.ButtonReleased(PSB_PAD_UP))
  {
    digitalWrite(WINDLASS_POWER, LOW);
    windlass.write(WINDLASS_STOP);
  }
  if (ps2x.ButtonPressed(PSB_PAD_DOWN))
  {
    windlass.write(180);
    digitalWrite(WINDLASS_POWER, HIGH);
  }
  if (ps2x.ButtonReleased(PSB_PAD_DOWN))
  {
    windlass.write(WINDLASS_STOP);
    digitalWrite(WINDLASS_POWER, LOW);
  }

  if (ps2x.ButtonPressed(PSB_R2)) digitalWrite(GUN_FIRE, HIGH);
  if (ps2x.ButtonReleased(PSB_R2)) digitalWrite(GUN_FIRE, LOW);

  if (ps2x.ButtonPressed(PSB_PAD_LEFT)) audioPrev();
  if (ps2x.ButtonPressed(PSB_PAD_RIGHT)) audioNext();
  if (ps2x.ButtonPressed(PSB_CIRCLE)) audioss();
  if (ps2x.ButtonPressed(PSB_SQUARE)) audiorep();
  
//Analogue Stick readings
  PlyStnRStickUpDn = ps2x.Analog(PSS_RY); //Right Stick Up and Down
  PlyStnRStickLtRt = ps2x.Analog(PSS_RX); //Right Stick Left and Right
  PlyStnLStickUpDn = ps2x.Analog(PSS_LY); //left Stick Up and Down
  PlyStnLStickLtRt = ps2x.Analog(PSS_LX); //Left Stick Left and Right

if(PlyStnRStickUpDn == 127)
{
  motor_2_set(STOP, 0);
}

if(PlyStnLStickUpDn == 127)
{
  motor_1_set(STOP, 0);
}


if(PlyStnLStickUpDn < 127)
{
  uint8_t power_to_set;
  power_to_set = map(PlyStnLStickUpDn, 128, 0, 0, 255);
  motor_1_set(BACKWARD, power_to_set);
}

if(PlyStnLStickUpDn > 127)
{
  uint8_t power_to_set;
  power_to_set = map(PlyStnLStickUpDn, 128, 255, 0, 255);
  motor_1_set(FORWARD, power_to_set);
}




if(PlyStnRStickUpDn < 127)
{
  uint8_t power_to_set;
  power_to_set = map(PlyStnRStickUpDn, 128, 0, 0, 255);
  motor_2_set(BACKWARD, power_to_set);
}

if(PlyStnRStickUpDn > 127)
{
  uint8_t power_to_set;
  power_to_set = map(PlyStnRStickUpDn, 128, 255, 0, 255);
  motor_2_set(FORWARD, power_to_set);
}



//Readings from PS2 Controller Sticks are from 0 to 255
//with the neutral being 128.  The zero positions are to
//the left for X-axis movements and up for Y-axis movements.

//Variables to carry the settings for the ESCs and Servos
//The values from the PS2 Sticks are mapped to 0 to 180 degrees 

//  ESCSetting = map(PlyStnRStickUpDn,-256,256,0,179);
//  StrServoSetting = map(PlyStnRStickLtRt,-256,256,0,179);
//  ServoN1Setting = map(PlyStnLStickUpDn,-256,256,0,179);
//  ServoN2Setting = map(PlyStnLStickLtRt,-256,256,0,179);


//Write it to the Servos or ESCs 

//  ESCcontrol.write(ESCSetting);
//  SteeringServo.write(StrServoSetting);
//  ServoN1.write(ServoN1Setting);
//  ServoN2.write(ServoN2Setting);


  digitalWrite(LEDpin,LEDHdlts); //Light the LED based on headlights status flag
  digitalWrite(LIGHTS,LEDHdlts);
  delay(15);
}
